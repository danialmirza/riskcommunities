# To run, try:
# python3 scrape.py
# It will download the data behind https://healthweather.us/
# and save it to per state csv files in the same folder.

import pandas as pd 
import json
import urllib.request


def scrapeState(state):
	with urllib.request.urlopen("https://static.kinsahealth.com/"+state+"_data.json") as url:
		data = json.loads(url.read())

	df = pd.DataFrame(data["data"], columns=data['columns'])

	df.to_csv(state+'.csv', index = None)

if __name__ == "__main__":
    # No data for Alaska, Hawaii
    states = ["AL", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL",
    "GA", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
    "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
    "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
    "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]
    for state in states:
        scrapeState(state)